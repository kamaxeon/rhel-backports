#!/bin/bash
set -euo pipefail

# Install packages using
# Args:
#   $1: Package name exectuable
#   $[2:]: List of packages to install
function install_packages {
    package_manager=$1
    shift
    "${package_manager}" -y install "${@}"
}

# Download and uncompress a tgz file
# Args:
#   $1: file URI
#   $2: Folder to uncompress
function download_and_uncompress_tgz {
    curl --silent --location "${1}" | tar --extract --gzip --file - --directory "${2}"
}

# Download and uncompress python
# Args:
#   $1: Python version
#   $2: Folder to uncompress
function download_and_uncompress_python {
    url="https://www.python.org/ftp/python/${1}/Python-${1}.tgz"
    dest_dir="${2}"
    download_and_uncompress_tgz "${url}" "${dest_dir}"
}

# Download and uncompress python
# Args:
#   $1: Bash version
#   $2: Folder to uncompress
function download_and_uncompress_bash {
    url="https://ftpmirror.gnu.org/bash/${1}.tar.gz"
    dest_dir="${2}"
    download_and_uncompress_tgz "${url}" "${dest_dir}"
}

# Compress and store artifacts
# Args:
#   $1: artifact name
#   $2: source folder
function store_artifact {
    # shellcheck disable=SC2154
    tar -zcf "${CI_PROJECT_DIR}/${1}.tgz" -C "${2}/" .
}

# Configure and install
# Args:
# $@ parameters to configure
function configure_and_install {
    ./configure "${@}"
    make
    make install
}
