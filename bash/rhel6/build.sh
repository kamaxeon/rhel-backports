#!/bin/bash
set -euo pipefail

source common_functions.sh

RPM_PACKAGE_MANAGER="yum"
RPM_PACKAGES=(
    tar
    gcc
    perl
)
SOURCES_DIR="/usr/src"
ARTIFACT_NAME="bash"
ARTIFACT_SRC="/opt"
CONFIGURE_OPTS=(--prefix="${ARTIFACT_SRC}")

install_packages "${RPM_PACKAGE_MANAGER}" "${RPM_PACKAGES[@]}"
# shellcheck disable=SC2154
download_and_uncompress_bash "${_BASH_VERSION}" "${SOURCES_DIR}"
# shellcheck disable=SC2154
cd "/usr/src/${_BASH_VERSION}"
configure_and_install "${CONFIGURE_OPTS[@]}"
store_artifact "${ARTIFACT_NAME}" "${ARTIFACT_SRC}"
