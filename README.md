# Backports for RHEL

This repo contains the scripts and CI jobs for building newer versions of
different pieces of software on **RHEL**, to be used by RHEL builders
container images at **CKI**.

## Software backported


* **Bash 4.4**: Only in RHEL6.
* **OpenSSL 1.0.2u**: Only in RHEL6.
* **Python 3.9.13**

## Architectures

* **RHEL6**: amd64
* **RHEL7**: amd64, ppc64le, s390x 
* **RHEL8**: amd64, arm64, ppc64le, s390x

The software compiled here will be stored as artifacts to be used at
build time in [CKI containers repository].

Using the feature [needs] from Gitlab CI, an artifact from this project can be
used from another project.

> NOTE: Using artifacts between projects is a premium feature. CKI uses it
> because is part of the [Gitlab Open Source program].

Job names follow this rule `build-<**rhel_version**>-<**architecture**>-<**backported_software**>`.

Here is an example of how to use it from another project:

```yaml
builder-rhel7-arm64:
  extends: [.publish_local, .internal_regs]
  needs:
    - project: cki-project/backports/rhel-backports
      job: build-rhel7-arm64-python
      ref: main
      artifacts: true
```

[CKI containers repository]: https://gitlab.com/cki-project/containers/
[needs]: https://docs.gitlab.com/ee/ci/yaml/README.html#cross-project-artifact-downloads-with-needs
[Gitlab Open Source program]: https://about.gitlab.com/solutions/open-source/
