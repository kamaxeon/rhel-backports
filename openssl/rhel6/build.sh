#!/bin/bash
set -euo pipefail

# shellcheck source-path=SCRIPTDIR
source common_functions.sh

RPM_PACKAGE_MANAGER="yum"
RPM_PACKAGES=(
    tar
    gcc
    perl
)
SOURCES_DIR="/usr/src"
ARTIFACT_NAME="openssl"
ARTIFACT_SRC="/opt/usr/local"

install_packages "${RPM_PACKAGE_MANAGER}" "${RPM_PACKAGES[@]}"
# shellcheck disable=SC2154
curl --silent --location "https://www.openssl.org/source/old/1.0.2/${OPENSSL_VERSION}.tar.gz" \
    | tar --extract --gzip --file - --directory "${SOURCES_DIR}"
# shellcheck disable=SC2154
cd "${SOURCES_DIR}/${OPENSSL_VERSION}"
./config shared
make
make install INSTALL_PREFIX=/opt/

store_artifact "${ARTIFACT_NAME}" "${ARTIFACT_SRC}"
